# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Virality prediction ###
 
* Version 0.02
* Descriptions: https://drive.google.com/open?id=1VDWkfiWN9gztPMiNYJYxl0N7SJAA4l9rPAYcv0L26m0

### Development plan and structure###

* Feature selection
** get_data() - get data from Youtube API.
** _search_list() - find content using list of query words.
** search_list() - turn pages to get all results.
** _video_list() - get features for video_id_list.
** video_list() - split video_id_list into batches of 50 items.

* Feature derivatives extruction

* Feature store
* ML model training
* Model evaluation

### Who do I talk to? ###

* Repo owner and admin Karpov Nikolay nkarpov@hse.ru
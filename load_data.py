from datetime import datetime
import requests
import json
import pandas as pd
import numpy as np
import isodate

api_key = "AIzaSyB9t3rQLSwHMWYW7soPiSpklxQhSE8FDfE"

def _search_list(q="", publishedAfter=None, publishedBefore=None, pageToken=""):
    # find content using list of query words.
    parameters = {"part": "id",
                  "maxResults": 50,
                  "order": "viewCount",
                  "pageToken": pageToken,
                  "q": q,
                  "type": "video",
                  "key": api_key,
                  }
    url = "https://www.googleapis.com/youtube/v3/search"

    if publishedAfter: parameters["publishedAfter"] = publishedAfter
    if publishedBefore: parameters["publishedBefore"] = publishedBefore

    page = requests.request(method="get", url=url, params=parameters)
    return json.loads(page.text)

def search_list(q="", publishedAfter=None, publishedBefore=None, max_requests=10):
    # turn pages to get all results.
    more_results = True
    pageToken=""
    results = []

    for counter in range(max_requests):
        j_results = _search_list(q=q, publishedAfter=publishedAfter, publishedBefore=publishedBefore, pageToken=pageToken)
        items = j_results.get("items", None)
        if items:
            results += [item["id"]["videoId"] for item in j_results["items"]]
            if "nextPageToken" in j_results:
                pageToken = j_results["nextPageToken"]
            else:
                return results
        else:
            return results
    return results

def _video_list(video_id_list):
    # get features for video_id_list.
    parameters = {"part": "statistics,contentDetails",
                  "id": ",".join(video_id_list),
                  "key": api_key,
                  "maxResults": 50
                  }
    url = "https://www.googleapis.com/youtube/v3/videos"
    page = requests.request(method="get", url=url, params=parameters)
    j_results = json.loads(page.text)
    df = pd.DataFrame([{**item["contentDetails"], **item["statistics"]} for item in j_results["items"]], dtype=np.int64)
    df["duration"]=df["duration"].apply(lambda x:isodate.parse_duration(x).total_seconds())
    df["video_id"] = [item["id"] for item in j_results["items"]]

    parameters["part"] = "snippet"
    page = requests.request(method="get", url=url, params=parameters)
    j_results = json.loads(page.text)
    df["publishedAt"] = [item["snippet"]["publishedAt"] for item in j_results["items"]]
    df["publishedAt"] = df["publishedAt"].apply(lambda x: datetime.strptime(x, "%Y-%m-%dT%H:%M:%S.000Z"))
    df["date"] = df["publishedAt"].apply(lambda x: x.date())
    # df["week"] = df["date"].apply(lambda x: x.isocalendar()[1])
    df["channelId"] = [item["snippet"]["channelId"] for item in j_results["items"]]
    df["title"] = [item["snippet"]["title"] for item in j_results["items"]]
    df["description"] = [item["snippet"]["description"] for item in j_results["items"]]
    df["channelTitle"] = [item["snippet"]["channelTitle"] for item in j_results["items"]]
    df["categoryId"] = [item["snippet"]["categoryId"] for item in j_results["items"]]
    return df

def video_list(video_id_list):
    # split video_id_list into batches of 50 items.
    values = []
    for index, item in enumerate(video_id_list[::50]):
        t_index = index * 50
        values.append(_video_list(video_id_list[t_index:t_index+50]))
    return pd.concat(values)

def get_data(queries, publishedAfter, publishedBefore):
    # get all data from Youtube API.
    results_list = []
    for q in queries:
        results = search_list(q=q,
                  publishedAfter=publishedAfter,
                  publishedBefore=publishedBefore,
                  max_requests=50)
        stat_data_set = video_list(results)
        stat_data_set["query"] = q
        results_list.append(stat_data_set)
    data_set = pd.concat(results_list)
    return data_set

if __name__ == "__main__":
    queries=["cute animals","kids"]
    data_set=get_data(queries, "2017-07-07T00:00:00Z", "2017-07-08T00:00:00Z")
    print(data_set)
